int sum_upto_single_digit(int);

int sum_upto_single_digit(int n) {
// ADD YOUR CODE HERE
int i, result = 0;
if(n < 0)
return -1;
else if(n >= 0){
for(i = 0; n != 0; i++) {
result = result + ( n % 10 );
n /= 10;
}
if(result >= 10)
return sum_upto_single_digit(result);
}
return result;
}

public class Palindrome01 {
    public static void main(String[] args) {
        int num = 878;
        System.out.println(isPalindrome(num));
    }

    public static int isPalindrome(int num) {
        //ADD YOUR CODE HERE
          int a;
          a = num;
         if (num > 99 && num < 1000){
            int rev = 0;
            int rem;

            while (num != 0){
                 rem = num % 10;
                 rev = rev * 10 + rem;
                 num = num / 10;
             }
          if (rev == a){
               return 1;
          }
         else{
            return 0;
         }
       }
        else{
         return -1;
        } 
}
}

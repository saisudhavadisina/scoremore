public class Armstrong01 {
    public static void main(String[] args) {
        int num1 = 100;
        int num2 = 125;
        System.out.println(generateArmstrongNums(num1, num2));
    }
    
    public static String generateArmstrongNums(int start, int limit) {
        if(start <= 0 || limit <= 0) {
            return "-1";
        } 
        if(start >= limit) {
            return "-2";
        }
        String res = "";
        for(int n = start; n <= limit ; n++) {
            if(isArmstrong(n)) {
                res += n + ",";
            }
        }
        if(res.length() == 0) {
            return "-3";
        }
        res = res.substring(0,res.length()-1); 
        return res;
    }

    public static boolean isArmstrong(int num) {
        return(num == sumOfPowersOfDigits(num));
    }

    public static int sumOfPowersOfDigits(int n) {
        int digits[] = getDigits(n);
        int sum = 0;
        for(int digit : digits) {
            sum +=power(digit,digits.length);
        }
        return sum;
    }

    public static int[] getDigits(int n) {
        String s = n + "";
        int[] digits = new int[s.length()];
        int idx = 0;
        while(n > 0) {
            digits[idx++] = n % 10;
            n /= 10;       
         }
         return digits;
    }

    public static int power(int d, int p) {
        int pow = 1;
        while(p > 0){
            pow *= d;
            p--;
        }
        return pow;
    }
}

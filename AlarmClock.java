public class AlarmClock {
    public static void main(String[] args) {
        int day_of_week = 3;
        boolean onVac = true;
        System.out.println(ringAlarm(day_of_week, onVac));
    }

   public static String ringAlarm(int dayOfWeek, boolean onVac) {
       if(dayOfWeek < 0 || dayOfWeek > 6) {
           return "Invalid Inputs";
       }
       String res = "";
       if(dayOfWeek >= 1 && dayOfWeek <= 5) {
           if(onVac) {
               res = "10:00";
           } else {
               res = "07:00";
           }
       }
       if(dayOfWeek == 0 || dayOfWeek == 6) {
           if(onVac) {
               res = "OFF";
           } else {
               res = "10:00";
           }
       }
     return res;   
    }
}

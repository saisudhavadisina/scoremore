int array_max(int[], int);
int array_min(int[], int);

int array_max(int ar[], int len) {
    // ADD YOUR CODE HERE
    int i;
    int max_element = ar[0];
    for( i = 1; i < len; i++ ) {
        if(max_element < ar[i]) {
            max_element = ar[i];
        }
    }
    return max_element;
}

int array_min(int ar[], int len) {
    // ADD YOUR CODE HERE
    int i;
    int min_element = ar[0];
    for( i = 1; i < len; i++ ) {
        if(min_element > ar[i]) {
            min_element = ar[i];
        }
    }
    return min_element;
}


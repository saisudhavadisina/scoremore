lowers = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
def shift_by(ch, by):
    return lowers[lowers.index(ch) + by]

def cumsum(shifts):
    return [sum(shifts[i:]) % 26 for i in range(len(shifts))]
def shifts(s, shifts):
    return ''.join(shift_by(a, b) for a, b in zip(s, cumsum(shifts)))
shifts = [3, 5, 9]
print(cumsum(a))
print(shifts(lowers, shifts))

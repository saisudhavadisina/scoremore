import java.util.*;
public class CharFrequency01 {
	public static void main(String[] args) {
		Map<Character, Integer> freqCount = frequencyCount("ELEMENTS");
        System.out.println(freqCount);
        
        for (Character key : freqCount.keySet()) {
            System.out.println(key + " " + freqCount.get(key));
        }  
        
	}
    public static Map<Character, Integer> frequencyCount(String str) {
       //ADD YOUR CODE HERE
     Map<Character, Integer> freq = new LinkedHashMap<Character, Integer>();
       if(str == null)
	       return freq;
       str = str.trim();
       str = str.toUpperCase();
       if(str.length() == 0)
	    return freq;
	int count;
	char ch1;
	char ch;
	for(int i = 0; i < str.length(); i++){
	    count = 1;
    	    if(Character.isLetter(str.charAt(i))){
		    ch = str.charAt(i); 
		    for(int j = 0; j < i; j++){
			     ch1 = str.charAt(j);
			    if(ch == ch1)
				    count++;
		    }
	    freq.put(ch,count);
	    }
	}
	return freq;
				    
    }
    }


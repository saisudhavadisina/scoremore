int get_text_file_size(char *);

int get_text_file_size(char *file_name) {
// ADD YOUR CODE HERE
FILE *fp = fopen(file_name, "r");
if(fp == NULL)
return -1;
fseek(fp, 0L, SEEK_END);
long int result = ftell(fp);

fclose(fp);

return result;
}

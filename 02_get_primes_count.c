int get_primes_count(int[], int);
int is_prime(int);

int is_prime(int num) {
    int i;
    int count;
    for (i = 1; i <= num; i++ ) {
        if( num % i == 0 ) {
            count++ ;
        }
    }
}

// n:number of elements in the array
int get_primes_count(int ar[], int n) {
    // ADD YOUR CODE HERE
    int i;
    int prime_count = 0;
    for( i = 0; i < n; i++ ) {
        if(is_prime(ar[i])) {
            prime_count++;
        }
    }
    return prime_count;
}

public class Fibonacci01 {
	public static void main(String[] args) {
	 	System.out.println(getFibonacciSeries(1));
	}
	public static String getFibonacciSeries(int num) {
         if(num < 1 || num > 40)
               return "-1";
         int a = 0;
         int b = 1;
         int res = 0;
         String s = new String("0");
         if(num < 3) {
                if(num == 1)
                     return s;
                else
                     return(s + "," + "1");
         } else if(2 < num && num < 41) {
                  num = num - 2;
                  s = (s + "," + "1");
                  while(num > 0) {
                          res = a + b;
                          s = (s + "," + Integer.toString(res));
                          a = b;
                          b = res;
                          num -= 1;
                  }
           }
           return s;
   }
}

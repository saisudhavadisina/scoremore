void reverse(int *,int);

void reverse(int *ar,int  n) {
// ADD YOUR CODE HERE
int i, j, temp;
for(i = 0, j = n - 1; i != j || i < j; i++, j--) {
temp = ar[i];
ar[i] = ar[j];
ar[j] = temp;
}
}

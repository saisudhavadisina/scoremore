public class SumOfDigits01 {
    public static void main(String[] args) {
        int num = 799;
        System.out.println(getSumOfDigits(num));
    }

    public static int getSumOfDigits(int num)	{
        int s = 0;
        if(num < 10 || num > 99) {
            return 0;
        }
        else {
            while(num != 0){
                 s = s + num % 10;
                 num = num / 10;
            }
            return s;
        }
    }
}
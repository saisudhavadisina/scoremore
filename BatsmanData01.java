import java.util.*;
import java.io.*;

class Batsman {
	private int id, noOfMatchesPlayed, noOfInningsPlayed, noOfTimesNotOut, noOfBallsFaced, totalRuns, highestScore,
			noOfCenturies, noOfHalfCenturies, noOfFours, noOfSixes;
	private String name;
	private double average, strikeRate;

	public Batsman() {
	}

	public Batsman(int id, String name, int noOfMatchesPlayed, int noOfInningsPlayed, int noOfTimesNotOut,
			int totalRuns, int highestScore, int noOfBallsFaced, int noOfCentures, int noOfHalfCenturies, int noOfFours,int noOfSixes) {
		this.id = id;
		this.name = name;
		this.noOfMatchesPlayed = noOfMatchesPlayed;
		this.noOfInningsPlayed = noOfInningsPlayed;
		this.noOfTimesNotOut = noOfTimesNotOut;
		this.totalRuns = totalRuns;
		this.highestScore = highestScore;
		this.noOfBallsFaced = noOfBallsFaced;
		this.noOfCenturies = noOfCentures;
		this.noOfHalfCenturies = noOfHalfCenturies;
		this.noOfFours = noOfFours;
		this.noOfSixes = noOfSixes;

		this.strikeRate = (double) this.totalRuns * 100 / this.noOfBallsFaced;
		this.average = (double) this.totalRuns / (this.noOfInningsPlayed - this.noOfTimesNotOut);
	}

    @Override
	public String toString() {
		return "Batsman [id=" + id + ", noOfMatchesPlayed=" + noOfMatchesPlayed + ", noOfInningsPlayed="
				+ noOfInningsPlayed + ", noOfTimesNotOut=" + noOfTimesNotOut + ", noOfBallsFaced=" + noOfBallsFaced
				+ ", totalRuns=" + totalRuns + ", highestScore=" + highestScore + ", noOfCenturies=" + noOfCenturies
				+ ", noOfHalfCenturies=" + noOfHalfCenturies + ", noOfFours=" + noOfFours + ", noOfSixes=" + noOfSixes
				+ ", name=" + name + ", average=" + average + ", strikeRate=" + strikeRate + "]";
	}
}

public class BatsmanData01 {
     
    public static void main(String[] args) throws IOException {
        String path = "./usercode/BatsmanData.txt";
        List<Batsman> batsmanList = readBatsmanData(path);
        for (Batsman b : batsmanList) {
            System.out.println(b); 
        }
    }
   
  	public static List<Batsman> readBatsmanData(String fileSource) throws IOException{
            BufferedReader br = new BufferedReader(new FileReader(fileSource));
            List<Batsman> ls = new ArrayList<Batsman>();
            String line = br.readLine();
            
            while (line != null) {
                String parts[] = line.split(",");
                Batsman b = new Batsman(con_str(parts[0]),parts[1],con_str(parts[2]),con_str(parts[3]),con_str(parts[4]),con_str(parts[5]),con_str(parts[6]),con_str(parts[7]),con_str(parts[8]),con_str(parts[9]),con_str(parts[10]),con_str(parts[11]));
                ls.add(b);
                line = br.readLine();

            }
            br.close();
         return ls;
        }
       
      public static int con_str(String s) {
              return Integer.parseInt(s);
      }

}

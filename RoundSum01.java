public class RoundSum01 {
    public static void main(String[] args) {
        int a = 23, b = 34, c = 69;
        System.out.println(sumOfMultiples(a, b, c));
    }

    public static int sumOfMultiples(int a, int b, int c) {
         //ADD YOUR CODE HERE
         if (a <= 0 || b <= 0 || c <= 0) {
             return -1;
         }
         int x,y,z;
         x = a + 9;
         while(a <= x) {
                  if(a % 10 == 0) {
                     break;
                  }
                  a += 1;
         }
         y = b + 9;
         while(b <= y) {
                  if(b % 10 == 0) {
                     break;
                  }
                  b += 1;
         }
         z = c + 9;
         while(c <= z) {
                  if(c % 10 == 0) {
                     break;
                  }
                  c += 1;
         }
         int sum;
         sum = a+b+c;
         return sum;
    }
}

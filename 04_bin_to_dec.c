int bin_to_dec(int);
int power(int, int);

int bin_to_dec(int num) {
// ADD YOUR CODE HERE
int decimal_Number = 0;
int i;
for(i = 0;num != 0;++i){
decimal_Number += (num % 10 * power(2,i));
num /= 10;
}
return decimal_Number;
}

int power(int x, int n) {
// ADD YOUR CODE HERE
if(n == 1)
return x;
else
return (x * power(x, n - 1));
}

public class OddPalindromes01 {
    public static void main(String[] args) {
        int num1 = 1500;
        int num2 = 2000;
        System.out.println(generateOddPalindromes(num1, num2));
    }

    public static String generateOddPalindromes(int start, int limit) {
        String res = "";
        if(start <= 0 || limit <= 0) {
            return "-1";
        }
        if(start >= limit) {
            return "-2";
        }
        
 
        for(int n = start; n <= limit; n++) {
            if(isPalindrome(n) && isAllDigitsOdd(n)){
                res += n + ",";
            }
        }
        if(res.length() == 0) {
            return "-3";
        }
        res = res .substring(0, res.length()-1);
        return res;
    }

    public static boolean isPalindrome(int num) {
        if(num == reverse(num)) {
            return true;
        } else {
            return false;
        }
    }

    public static int reverse(int num) {
        int temp = num;
        int rev = 0;
        while(temp != 0) {
            rev = (rev * 10) + (temp % 10);
            temp = temp / 10;
        }
        return rev;
    }
    public static boolean isAllDigitsOdd(int num) {
        while(num > 0) {
            int rem = num % 10;
            if(rem != 0) {
                if(rem % 2 == 0) {
                    return false;
                }
            }
            num /= 10;
        }
        return true;
    }
}


public class Customer {
	private String customerName;
	private int creditPoints;
	public Customer(String customerName, int creditPoints) {
		super();
		this.customerName = customerName;
		this.creditPoints = creditPoints;
	}
	public int getCreditPoints() {
		return creditPoints;
	}
	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + "]";
	}
}

class CardType {
	// ADD YOUR CODE HERE
	private Customer customer;
	private String cardType;
	
	public CardType(Customer customer, String cardType) {
		super();
		this.customer = customer;
		this.cardType = cardType;
	}

	@Override
	public String toString() {
		return "The Customer '" + customer + " Is Eligible For " + cardType + "' Card.";
	}
	
}

class CardsOnOffer  {
	// ADD YOUR CODE HERE
	public static CardType getOfferedCard(Customer customer){
	String cardType =""; 
		if (customer.getCreditPoints() >= 100 && customer.getCreditPoints() <= 500){
			cardType = "Silver";
			
		}else if(customer.getCreditPoints() >= 501 && customer.getCreditPoints() <= 1000){
			cardType ="Gold";
		}else if (customer.getCreditPoints() > 1000){
			cardType ="Platinum";
		}else if (customer.getCreditPoints() < 100){
			cardType="EMI";
		}
		CardType cardDetails = new CardType(customer,cardType);
	return cardDetails;
	}
}

class sMain {
	public static void main(String[] args) {
		// ADD YOUR CODE HERE
		Customer customer1 = new Customer("Rajeev", 506);
		CardType type1 = CardsOnOffer.getOfferedCard(customer1);
		System.out.println(type1);
	}
}
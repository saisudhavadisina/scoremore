def shiftingLetters(string, shifts):
    length = len(shifts)
    result = list(string)[:length]
    a = ord('a')
    for i in range(1, len(shifts)):
        shifts[length - 1 - i] += shifts[length - i]
    for i in range(len(shifts)):
        result[i] = chr((ord(result[i]) - a + shifts[i]) % 26 + a)
    return ''.join(result)
input_string = str(input())
shifts = [int(i) for i in input().split(' ')]
print(shiftingLetters(input_string, shifts))


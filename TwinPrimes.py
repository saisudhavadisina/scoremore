def isPrime(num):
    #ADD YOUR CODE HERE
    count = 0
    if num <= 0:
        return "Invalid Input"
    else: 
        for i in range(2,num):
            if(num % i == 0):
                count += 1
        if count == 0:
            return True
        else:
            return False

def twinPrimes(start, limit):
    #ADD YOUR CODE HERE
    if start <= 0 or limit <= 0:
        return -1
    elif start >= limit:
        return -2
    count = 0
    for i in range(start, limit):
        if isPrime(i) and isPrime(i + 2):
            count += 1
            print("{:d}, {:d}".format(i, i + 2))
         #   return (i, i + 2)
    if count == 0 :
        return -3
    #else :
     #   print(i, i + 2)
print(twinPrimes(8, 10))

bool is_prime(int);
bool is_twin_primes(int, int);

bool is_prime(int num) {
//ADD YOUR CODE HERE
int i, primecount;
for( i = 1; i <= num; i++ ){
if( num % i == 0)
primecount++;
}

bool twin_primes(int num1, int num2) {
//ADD YOUR CODE HERE
if( is_prime(num1) && is_prime(num2) && (abs(num1 - num2) == 2))
return true;
else
return false;
}

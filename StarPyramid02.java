public class StarPyramid02 {
    public static void main(String[] args) {
        System.out.print(starPattern(5));
    }

	public static String starPattern(int rows) {
		 if(rows < 0)
             return "-1";
         if(rows == 0)
             return "-2";
         String result = "";
         for(int i = 0; i < rows; i++){
             for(int k = rows - 1; k > i; k--)
                 result += " ";
             for(int j = 1; j <= ((2 * i) + 1); j++)
                 result += "*";
             result += "\n";
         }
         return result;
    }
}

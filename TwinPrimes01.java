public class TwinPrimes01 {
    public static void main(String[] args) {
        int num1 = 1;
        int num2 = 200;
        System.out.println(twinPrimes(num1, num2));
    }

    public static String twinPrimes(int start, int limit) {
        if(start <= 0 || limit <= 0) {
            return "-1";
        }
        if(start >= limit) {
            return "-2";
        }
        String res = "";
        int p_prime = 2;
        for(int n = start; n <= limit; n++) {
            if(isPrime(n)) {
               if(n - p_prime == 2) {
                   res += p_prime + ":" + n + ",";
               }
               p_prime = n;
            }
        }
        if(res.length() == 0) {
            return "-3";
        }
        res = res.substring(0,res.length()-1);
        return res;
    }

    public static boolean isPrime(int num) {
        if(num == 1) {
            return false;
        }
        if(num == 2) {
            return true;
        }
        if(num % 2 == 0){
            return false;
        }
        for(int i = 2; i <= num/2; i++ ) {
            if(num % i == 0){
                return false;
            }        
        }
        return true;
    }
}

int get_digit_position(int, int);

int get_digit_position(int num, int digit) {
//ADD YOUR CODE HERE
int i;
    if(num < 0) {
        return -1;
    } else if(digit >= 10) {
        return -2;
    } else {
        for( i = 0; num != 0; i++) {
            if(num % 10 == digit)
                return pow(10, i);
            else
                num /= 10;
        }
    }
}

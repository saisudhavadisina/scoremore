public class FourPerLine01 {
    public static void main(String[] args) {
        int num = 20;
        System.out.println(getFourPerLine(num));
    }

	public static String getFourPerLine(int num) {
         if(num < 1 || num > 99)
             return "-1";
         String result = "";
         for(int i = 1; i <= num; i++){
             if(i % 4 == 0){
                 if(i <= 9)
                     result += " " + i + "\n";
                 else
                     result += i + "\n";
             } else {
                 if(i <= 9)
                     result += " " + i + " ";
                 else if(i == num)
                     result += i;
                 else
                     result += i + " ";
             }
         }
         return result;
	}
}

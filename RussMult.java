public class RussMult01 {
    public static void main(String[] args) {
        int n1 = 25475;
        int n2 = 35655;
        System.out.println(getProduct(27, 35));
    }
    public static String getProduct(int num1, int num2) {
        if(num1 <= 0 || num2 <= 0) {
            return "-1";
        }
        if(num1 >= 100000 || num2 >= 100000) {
            return "-2";
        }
        int sum = 0;
        String res = "";
        boolean first = true;
        while(num1 > 0) {
            if(num1 % 2 != 0){
                sum += num2;
                if(first) {
                    res += num2;
                    first = false;
                } else {
                      res += "+" + num2;
                }
            }
            num1 = num1/2;
            num2 = num2*2;
        }
        res = res + " = " + sum;
        return res;
    }
}

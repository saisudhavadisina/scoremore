def sum_of_divisors(num):
       sum = 0
       
       for i in range (1,num):
           if num % i == 0:
             sum = sum + i
       return sum     

def type_of_number(num):
       if num <= 0: 
         return "Invalid Input"
       elif sum_of_divisors(num)==num:
         return "Perfect Number" 
       elif sum_of_divisors(num)<num:
         return "Deficient Number"
       else:
         return "Abundant Number"
   
